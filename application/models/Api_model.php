<?php
class Api_model extends CI_Model
{
    public function getOptionsFromApi()
    {
        $url = 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_pewangi'; // Ganti dengan URL endpoint REST API Anda
        $response = $this->curl->simple_get($url);
        $data = json_decode($response, true);

        if ($data) {
            $options = array();
            foreach ($data as $item) {
                $options[$item['id_pewangi']] = $item['nama_pewangi'];
            }
            return $options;
        } else {
            return array();
        }
    }
}
