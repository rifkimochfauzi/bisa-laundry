<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('soft/'); ?>/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= base_url('soft/'); ?>/assets/img/favicon.png">
    <title>
        Bisa.Laundry
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="<?= base_url('soft/'); ?>/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="<?= base_url('soft/'); ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="<?= base_url('soft/'); ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="<?= base_url('soft/'); ?>/assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" />
    <link href="<?= base_url('asset/'); ?>/css/boostrap.css'" rel="stylesheet" />

    <link href="<?= base_url('NiceAdmin/'); ?>assets/css/style.css" rel="stylesheet">
    <!-- Pastikan Anda sudah memuat jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <!-- Memuat pustaka DataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>


    <style>
    .Diproses {
        background-color: yellow;
    }

    .Antrian {
        background-color: grey;
        color: white;
    }

    .Selesai {
        background-color: green;
        color: white;
    }

    .Dikembalikan {
        background-color: red;
        color: white;
    }
    </style>
</head>