<form action="<?= base_url("profilecs/edit"); ?>" method="POST" enctype="multipart/form-data">
    <div class="container-fluid py-4">
        <div class="row container">
            <div class="col-md-8">
                <div class="card">
                    <?php
                    foreach ($data as $row) : ?>
                        <div class="card-header pb-0">
                            <div class="d-flex align-items-center">
                                <p class="mb-0">Profile</p>
                                <a href="<?= base_url('profilecs/edit/' . $row['id_customer']) ?>" class="btn btn-info btn-sm ms-auto" type="submit" value="simpan">Edit</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <p class="text-uppercase text-sm">User Information</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Nama </label>
                                        <input class="form-control" type="text" value="<?= $row['nama_customer'] ?>" name="nama_customer">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">No Telp</label>
                                        <input class="form-control" type="text" value="<?= $row['nomor_customer'] ?>" name="nomor_customer">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Email</label>
                                        <input class="form-control" type="email" value="<?= $row['email'] ?>" name="email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Password</label>
                                        <input class="form-control" type="password" value="<?= $row['password'] ?>" name="password">
                                    </div>
                                </div>
                            </div>
                            <hr class="horizontal dark">
                            <p class="text-uppercase text-sm">Contact Information</p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Alamat</label>
                                        <input class="form-control" type="text" value="<?= $row['alamat_customer'] ?>" name="alamat_customer">
                                    </div>
                                </div>
                                <!-- <div class="col-md-6"> -->
                                <!-- <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Tanggal Lahir</label>
                                        <input class="form-control" type="text" value="New York">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Jenis Kelamin</label>
                                        <input class="form-control" type="text" value="laki laki" name="">
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</form>