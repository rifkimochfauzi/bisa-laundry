<body>
    <div class="container"><br />
        <h5>Item Pruduct Laundry</h5>
        <div class="row">
            <div class="col-md-6">
                <!-- <div class="card mb-4"> -->
                <div class="row">
                    <?php foreach ($data1->result() as $ro) : ?>
                        <div class="col-md-4">
                            <div class="thumbnail">
                                <br>
                                <div class="card-deck">
                                    <div class="card">
                                        <img src="<?= base_url('uploads/operator') ?>/<?= $ro->Foto_item ?> " width="70px">
                                        <div class="caption ">

                                            <h6><?php echo $ro->nama_item; ?></h6>
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <h6><?php echo number_format($ro->harga); ?></h6>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="number" name="quantity" id="<?php echo $ro->id_item; ?>" value="1" class="quantity form-control">
                                                </div>
                                            </div>
                                            <button class="add_cart btn btn-success btn-block" data-productid="<?php echo $ro->id_item; ?>" data-productname="<?php echo $ro->nama_item; ?>" data-productprice="<?php echo $ro->harga; ?>">Add To Cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!-- </div> -->
            </div>
            <div class="col-md-4">
                <h4>keranjang</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Items</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody id="detail_cart">

                    </tbody>

                </table>
            </div>
        </div>
    </div>
    </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/js/jquery-3.2.1.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js' ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.add_cart').click(function() {
                var id_item = $(this).data1("productid");
                var nama_item = $(this).data1("productname");
                var harga = $(this).data1("productprice");
                var quantity = $('#' + id_item).val();
                $.ajax({
                    url: "<?php echo site_url('pembayarancs/add_to_'); ?>",
                    method: "POST",
                    data1: {
                        id_item: id_item,
                        nama_item: nama_item,
                        harga: harga,
                        quantity: quantity
                    },
                    success: function(data1) {
                        $('#detail_cart').html(data1);
                    }
                });
            });

            $('#detail_cart').load("<?php echo site_url('pembayarancs/load_'); ?>");

            $(document).on('click', '.romove_cart', function() {
                var row_id1 = $(this).attr("id");
                $.ajax({
                    url: "<?php echo site_url('pembayarancs/delete_'); ?>",
                    method: "POST",
                    data1: {
                        row_id1: row_id1
                    },
                    success: function(data1) {
                        $('#detail_cart').html(data1);
                    }
                });
            });
        });
    </script>