<div class="card text-center" style="width: 400px; position: absolute;margin-top: -200px;margin-left: -200px;left: 50%;top: 50%;width: 400px;height: 430px;">
    <div class="card-header h5 text-white bg-info">Lupa Password</div>
    <div class="card-body px-5">
        <p class="card-text py-2">
            Masukan email anda dengan benar
        </p>
        <div class="form-outline">
            <label class="form-label" for="typeEmail">Email</label>
            <input type="email" id="typeEmail" class="form-control my-3" />

        </div>
        <a href="<?= base_url('login'); ?>" class="btn btn-info w-100">Reset password</a>
        <div class="d-flex justify-content-between mt-4">
            <a class="" href="<?= base_url('login'); ?>">Login</a>
            <a class="" href="<?= base_url('login/regist'); ?>">Register</a>
        </div>
    </div>
</div>