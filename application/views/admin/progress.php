<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-7">
                            <h6>Progress</h6>
                        </div>
                        <div class=" col-lg-6 col-5 my-auto text-end">
                            <form method="post" action="<?php echo base_url('progress/search_data'); ?>">
                                <input type="text" name="keyword" placeholder="Type here...">
                                <input type="submit" value="Search">
                            </form>
                            <!-- <div class="input-group">

                                <span class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" placeholder="Type here...">
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">

                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama
                                    </th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Tanggal transaksi</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Status</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Detail Transaksi</th>
                                    <th class="text-secondary opacity-7"></th>
                                </tr>
                            </thead>

                            <form id="update-form" action="<?= base_url("progress/update"); ?>" method="POST"
                                enctype="multipart/form-data">
                                <tbody>
                                    <?php $no = 1;
                                    foreach ($order['data'] as $ord) : ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm"><?= $ord['nama_customer'] ?></h6>
                                                    <p class="text-xs text-secondary mb-0"><?= $ord['no_hp'] ?></p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0"><?= $ord['waktu_cucian_masuk'] ?>
                                            </p>
                                            <!-- <p class="text-xs text-secondary mb-0">Projects</p> -->
                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            <h6 class=" <?= $ord['status_cucian'] ?> mb-0 text-sm">
                                                <?= $ord['status_cucian'] ?>
                                            </h6>
                                        </td>
                                        <td class="align-middle text-center">
                                            <a href="<?= base_url('riwayat/invoice/' . $ord['id_cucian']) ?>"><span>Lihat
                                                    invoice...</span></a>
                                        </td>
                                        <td class="align-middle">
                                            <a href="<?= base_url('progress/edit/' . $ord['id_cucian']) ?>"
                                                class="btn btn-info btn-sm">Edit</a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </form>

                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-left">
                                <?php echo $pagination; ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>