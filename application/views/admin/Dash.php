<!-- Hero -->
<style>
.badge {
    padding: 5px;
    border-radius: 5px;
    font-weight: bold;
}

.badge-diproses {
    background-color: yellow;
    color: black;
}

.badge-selesai {
    background-color: green;
    color: white;
}

.badge-antrian {
    background-color: blue;
    color: white;
}
</style>
<div class="p-4 shadow-4 rounded-3" style="
    background-image: url('asset/laundry-header.png'); ">
    <h2 style="text-align: center;">Buat layanan laundry</h2>
    <h2 style="text-align: center;">
        Terbaru
    </h2>
    <hr class="my-4" />
    <center><button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#smallModal2"
            style="justify-content:center;">
            Buat Layanan
        </button>
    </center>
    <div class=" modal fade" id="smallModal2" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Buat Layanan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <?php echo form_open_multipart('Item/tambah_aksi'); ?>
                    <!-- <div class="col-md-12">
                        <label for="example-text-input">Gambar
                            Item</label>
                        <input type="file" name="Foto_item" size="20" accept="image/*" class="form-control">
                    </div> -->
                    <div class="col-md-12">
                        <label for="example-text-input">Nama
                            Layanan</label>
                        <input type="text" name="nama_item" class="form-control" required="">
                        <?= form_error('nama_item', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="col-md-12">
                        <label for="example-text-input" class="form-control-label">Satuan</label>
                        <select id="inputState" class="form-select" name="tipe_item">
                            <option value="Kg">Kg</option>
                            <option value="satuan/pcs">satuan/pcs</option>
                        </select>
                        <?= form_error('tipe_item', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="col-md-12">
                        <label for="example-text-input" class="form-control-label">Harga</label>
                        <input type="text" name="harga" class="form-control" required="">
                        <?= form_error('harga', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div><!-- End Small Modal-->
        <!-- <button type="button" class="btn btn-info" style="position:relative;left:420px;" data-bs-target="#smallModal2">
        Buat Layanan
    </button> -->
    </div>
</div>
<!-- Hero -->
<div class="row my-4">
    <div class="col-lg-8 col-md-6 mb-md-0 mb-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col-lg-6 col-7">
                        <h6>Progress</h6>
                    </div>
                    <div class=" col-lg-6 col-5 my-auto text-end">
                        <form method="post" action="<?php echo base_url('home/search_data'); ?>">
                            <input type="text" name="keyword" placeholder="Type here...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive">
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Tanggal transaksi</th>
                                <th
                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    Status</th>
                                <th
                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    Detail Transaksi</th>
                                <th class="text-secondary opacity-7"></th>
                            </tr>
                        </thead>
                        <?php $no = 1;
                        // var_dump($item);
                        foreach ($order['data'] as $ord) : ?>
                        <form id="update-form" action="<?= base_url('home/update'); ?>" method="POST"
                            enctype="multipart/form-data">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1">
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm"><?= $ord['nama_customer'] ?></h6>
                                                <p class="text-xs text-secondary mb-0"><?= $ord['no_hp'] ?></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0"><?= $ord['waktu_cucian_masuk'] ?>
                                        </p>
                                        <!-- <p class="text-xs text-secondary mb-0">Projects</p> -->
                                    </td>
                                    <td class="align-middle text-center text-sm">
                                        <h6 class=" <?= $ord['status_cucian'] ?> mb-0 text-sm">
                                            <?= $ord['status_cucian'] ?>
                                        </h6>
                                        <!-- 
                                        <span
                                            class="status-label badge badge-sm bg-gradient-success"><?= $ord['status_cucian'] ?></span> -->

                                    </td>
                                    <td class="align-middle text-center">
                                        <a href="<?= base_url('riwayat/invoice/' . $ord['id_cucian']) ?>"><span>Lihat
                                                invoice...</span></a>
                                    </td>
                                    <td class="align-middle">
                                        <a href="<?= base_url('home/edit/' . $ord['id_cucian']) ?>"
                                            class="btn btn-info btn-sm">Edit</a>
                                    </td>
                                </tr>
                            </tbody>
                        </form>
                        <?php endforeach ?>
                    </table>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-left">
                            <?php echo $pagination; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="card-body p-3">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <?php echo form_open_multipart('home/tambah_aksi'); ?>
                    <h5 class="card-title" style="text-align: center;">Masukan jenis pewangi</h5>
                    <p class="card-text" style="text-align: center;">Atur ketersediaan pewangi di Bisa Laundry</p>
                    <button type="button" class="btn btn-secondary" data-bs-toggle="modal"
                        data-bs-target="#smallModal1">
                        Buat Stok
                    </button>
                    <div class="modal fade" id="smallModal1" tabindex="-1">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buat Stok</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Masukan jenis
                                            pewangi</label>
                                        <input class="form-control" type="text" name="nama_pewangi">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Kembali</button>
                                    <button type="submit" class="btn btn-info">simpan</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- End Small Modal-->
                    <a href="<?= base_url('pewangi') ?>" class="btn btn-info" style="position:relative;left:20px;">Liat
                        Stok</a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

</div>
<script>
$('#smallModal').on('shown.bs.modal', function(event) {
    var button = $(event.relatedTarget); // Tombol yang memicu modal
    var id_cucian = button.data('id_cucian'); // Mengambil nilai ID dari atribut data
    console.log('ID yang akan diubah: ' + id_cucian);

    // Lakukan tindakan lain dengan ID ini, seperti memuat data ke dalam modal
});
</script>