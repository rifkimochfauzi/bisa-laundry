<?php echo $this->session->flashdata('pesan'); ?>
<div class="col-lg-6 col-7">
    <?php echo $this->session->flashdata('message'); ?>
</div>
<a href="<?php echo base_url('Item/tambah') ?>" class="btn btn-info"><i class="fa fa-plus"></i>Tambah Item</a>
<!-- <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class=" navbar-form navbar-right">
                <form action="<?php echo site_url('item/search'); ?>" method="get">
                    <input type="text" name="keyword" placeholder="Kata Kunci">
                    <button type="submit">Cari</button>
                </form>
            </div>
        </div> -->

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col-lg-6 col-7">
                        <h6>Layanan Laundry</h6>
                    </div>
                    <div class=" col-lg-6 col-5 my-auto text-end">
                        <form method="post" action="<?php echo base_url('item/search_data'); ?>">
                            <input type="text" name="keyword" placeholder="Type here...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
                <div class="table-responsive p-0">
                    <table class=" table datatable align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Item
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Nama Item</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Type Item</th>
                                <th
                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    Harga</th>
                                <th class="text-secondary opacity-7"> Action</th>
                            </tr>
                        </thead>
                        <?php $no = 1;
                                // var_dump($item);
                                foreach ($item['data'] as $itm) : ?>
                        <tbody>
                            <tr>
                                <td>
                                    <div><?= $no++ ?></div>
                                </td>
                                <td>
                                    <div class="d-flex px-2 py-1">
                                        <div>
                                            <!-- <img src="<?= base_url('uploads/operator') ?>/<?= $itm['nama_foto'] ?> " width="70px"> -->
                                            <img src="<?= base_url('asset/bukukas.png') ?>" width="70px">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <?= $itm['nama_item'] ?>
                                </td>
                                <td>
                                    <?= $itm['tipe_item'] ?>
                                </td>
                                <td class="align-middle text-center text-sm">
                                    <?= $itm['harga'] ?>
                                </td>

                                <td>
                                    <a href="<?= base_url('item/edit/' . $itm['id_item']) ?>"
                                        class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                    <a href="<?= base_url('item/delete/' . $itm['id_item']) ?>"
                                        class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        <?php endforeach ?>
                    </table>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-left">
                            <?php echo $pagination; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>