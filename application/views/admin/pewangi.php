<div class="row">
    <div class="col-lg-8 col-md-6 mb-md-0 mb-4">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col-lg-6 col-7">
                        <h6>Layanan Laundry</h6>
                    </div>
                    <div class=" col-lg-6 col-5 my-auto text-end">
                        <form method="post" action="<?php echo base_url('pewangi/search_data'); ?>">
                            <input type="text" name="keyword" placeholder="Type here...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Nama pewangi</th>
                                <th class="text-secondary opacity-7"> Action</th>
                            </tr>
                        </thead>
                        <?php $no = 1;
                        // var_dump($item);
                        foreach ($pewangi['data'] as $pw) : ?>
                        <tbody>
                            <tr>
                                <td>
                                    <div><?= $no++ ?></div>
                                </td>
                                <td>
                                    <?= $pw['nama_pewangi'] ?>
                                </td>
                                <td>
                                    <a href="<?= base_url('pewangi/edit/' . $pw['id_pewangi']) ?>"
                                        class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                    <a href="<?= base_url('pewangi/delete/' . $pw['id_pewangi']) ?>"
                                        class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        <?php endforeach ?>
                    </table>
					<nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-left">
                            <?php echo $pagination; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="card-body p-3">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <?php echo form_open_multipart('pewangi/tambah_aksi'); ?>
                    <h5 class="card-title" style="text-align: center;">Masukan jenis pewangi</h5>
                    <p class="card-text" style="text-align: center;">Atur ketersediaan pewangi di Bisa Laundry</p>
                    <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#smallModal1">
                        Buat Stok
                    </button>
                    <div class="modal fade" id="smallModal1" tabindex="-1">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buat Stok</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Masukan jenis
                                            pewangi</label>
                                        <input class="form-control" type="text" name="nama_pewangi">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Kembali</button>
                                    <button type="submit" class="btn btn-info">simpan</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- End Small Modal-->
                    <a href="<?= base_url('home') ?>" class="btn btn-secondary"
                        style="position:relative;left:20px;">Kembali</a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
