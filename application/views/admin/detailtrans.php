<div class="container-fluid py-2" style="display: flex; justify-content:center; width:50%;">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Transaksi</p>
                        <a href="<?php echo base_url('pembayaran') ?>" class="btn btn-info btn-sm ms-auto"><i
                                class="fa fa-sign-out" aria-hidden="true"></i> Back</a>
                    </div>
                </div>
                <div class=" card-body">
                    <!-- Multi Columns Form -->
                    <form class="row g-3" action="<?= base_url("pembayaran/tambah_aksi"); ?>" method="POST"
                        enctype="multipart/form-data">
                        <div class="col-md-6">
                            <!-- <label for="inputName5" class="form-label">Tipe Layanan</label> -->
                            <input type="hidden" class="form-control" id="inputName5" name="nama_layanan"
                                value="<?php echo $this->input->get('layanan'); ?> (<?php echo $this->input->get('satuan'); ?>)">
                        </div>
                        <div class=" col-md-6">
                            <!-- <label for="inputName5" class="form-label">berat</label> -->
                            <input type="hidden" class="form-control" id="inputName5" name="jumlah_layanan"
                                value="<?php echo $this->input->get('qty'); ?>">
                        </div>
                        <div class="col-md-12">
                            <label for="inputName5" class="form-label">Nama Pelanggan</label>
                            <input type="text" class="form-control" id="inputName5" name="nama_customer">
                        </div>
                        <div class="col-md-12">
                            <label for="inputName5" class="form-label">Alamat Pelanggan</label>
                            <input type="text" class="form-control" id="inputName5" name="alamat_customer">
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Gread Total</label>
                                <input class="form-control" type="text" name="total_harga"
                                    value="<?php echo $this->input->get('total'); ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="inputName5" class="form-label">No hp pelanggan</label>
                            <input type="text" class="form-control" id="inputName5" name="no_hp">
                        </div>
                        <div class="col-md-6">
                            <label for="inputState" class="form-label">jenis pewangi</label>
                            <select id="inputState" class="form-select" name="id_pewangi">
                                <?php foreach ($pewangi['data'] as $pw) : ?> {
                                <option value="<?= $pw['id_pewangi'] ?>"> <?= $pw['nama_pewangi'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="inputDateTime" class="form-label">Estimasi selesai</label>
                            <input type="datetime-local" class="form-control" name="waktu_cucian_selesai">
                        </div>
                        <div class="col-md-6">
                            <label for="inputState" class="form-label">metode pembayaran</label>
                            <select id="inputState" class="form-select" name="metode_pembayaran">
                                <option selected value="Cash">cash</option>
                                <option value="Transfer">transfer</option>
                            </select>
                            <input type="hidden" class="form-control" id="inputName5" name="status_cucian"
                                value="Antrian">
                        </div>

                        <br><br>
                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
                        <button type="reset" class="btn btn-danger btn-sm"><i class="fa fa-save"></i>Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- -->