<hr>
<hr>
<section class="section">
    <div class="row" style=" justify-content:center; ">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Edit Item</p>
                        <a href="<?php echo base_url('progress/index') ?>" class="btn btn-info btn-sm ms-auto"><i
                                class="fa fa-sign-out" aria-hidden="true"></i> Close</a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Ubah Status Cucian</h5>
                    <!-- Vertical Form -->
                    <?php foreach ($order['data'] as $ord) : ?>
                    <form action="<?= base_url("progress/update"); ?>" method="POST" enctype="multipart/form-data"
                        class="row g-3">

                        <div class="col-sm-10">
                            <div class="form-check">
                                <input type="hidden" name="id_cucian" value="<?= $ord['id_cucian'] ?>">
                                <input class="form-check-input" type="radio" name="status_cucian" value="Diproses">
                                <label class="form-check-label" for="gridRadios1">
                                    diproses
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="status_cucian" value="Selesai">
                                <label class="form-check-label" for="gridRadios2">
                                    selesai
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="status_cucian" value="Dikembalikan">
                                <label class="form-check-label " for="gridRadios2" font color='red'>
                                    Dikembalikan
                                </label>
                            </div>
                        </div>
                        <h5 class="card-title">Ubah Data pelanggan</h5>
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Ubah Data,Tekan disini!!
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <label for="inputNanme4" class="form-label">Nama Pelanggan</label>
                                                <input class="form-control" type="text" name="nama_customer"
                                                    value="<?= $ord['nama_customer'] ?>">
                                            </div>
                                            <div class="col-6">
                                                <label for="inputEmail4" class="form-label">Email</label>
                                                <input class="form-control" type="text" name="alamat_customer"
                                                    value="<?= $ord['alamat_customer'] ?>">
                                            </div>
                                            <div class="col-6">
                                                <label for="inputNanme4" class="form-label">No Hp</label>
                                                <input class="form-control" type="text" name="no_hp"
                                                    value="<?= $ord['no_hp'] ?>">
                                            </div>
                                            <div class="col-6">
                                                <label for="inputEmail4" class="form-label">Metode Pembayaran</label>
                                                <input class="form-control" type="text" name="metode_pembayaran"
                                                    value="<?= $ord['metode_pembayaran'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-secondary">Reset</button>
                            </div>
                        </div>
                    </form><!-- Vertical Form -->
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</section>