<style>
/* CSS untuk mencetak hanya div dengan class "print-content" */
@media print {
    body * {
        visibility: hidden;
        header: hidden;
    }

    .print-content,
    .print-content * {
        visibility: visible;
        size: A4;
        margin: 0;
        left: 200px
    }



}
</style>
<div class="row">
    <div class="col-lg-6 col-7">
        <button class="btn btn-light text-capitalize border-0" data-mdb-ripple-color="dark" onclick="window.print()"><i
                class="fas fa-print text-info"></i>Print</button>

    </div>
    <div class=" col-lg-6 col-5 my-auto text-end">
        <form method="post" action="<?php echo base_url('item/search_data'); ?>">
            <input type="text" name="keyword" placeholder="Type here...">
            <input type="submit" value="Search">
        </form>
    </div>
</div>
<br>

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col-lg-6 col-7">
                        <h6>Hasil Laporan Pembayaran dan pendapatan</h6>
                    </div>
                    <!-- <div class=" col-lg-6 col-5 my-auto text-end">
                        <form method="post" action="<?php echo base_url('item/search_data'); ?>">
                            <input type="text" name="keyword" placeholder="Type here...">
                            <input type="submit" value="Search">
                        </form>
                    </div> -->
                </div>
            </div>
            <div class="print-content">
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0 table-paginate">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama
                                        Costumer
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Waktu
                                        cucian masuk
                                    </th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        waktu cucian selesai</th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Metode pembayaran</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        pembayaran
                                    </th>

                                </tr>
                            </thead>
                            <?php $no = 1;
                                // var_dump($item);
                                foreach ($laporan ['data'] as $lp) : ?>
                            <tbody>
                                <tr>
                                    <td>
                                        <div><?= $no++ ?></div>
                                    </td>

                                    <td>
                                        <?= $lp['nama_customer'] ?>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0"> <?= $lp['waktu_cucian_masuk'] ?></p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0"> <?= $lp['waktu_cucian_selesai'] ?></p>
                                    </td>

                                    <td>
                                        <?= $lp['metode_pembayaran'] ?>
                                    </td>
                                    <td>
                                        <?= $lp['total_harga'] ?>
                                    </td>
                                </tr>
                            </tbody>
                            <?php endforeach ?>
                        </table>
                        <!-- Buat navigasi pagination -->
                        <!-- <div class="pagination-links">
                        <?php echo $this->pagination->create_links(); ?>
                    </div> -->
                    </div>
                </div>
                <h6>Total pemasukan : <?= $laporan["total_pemasukan"] ?>
                </h6>
            </div>
        </div>
    </div>
</div>