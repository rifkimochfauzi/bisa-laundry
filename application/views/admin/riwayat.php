<div class="col-lg-6 col-7">
    <?php echo $this->session->flashdata('message'); ?>
</div>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-7">
                            <h6>Riwayat pemesan</h6>
                        </div>
                        <div class=" col-lg-6 col-5 my-auto text-end">
                            <form method="post" action="<?php echo base_url('riwayat/search_data'); ?>">
                                <input type="text" name="keyword" placeholder="Type here...">
                                <input type="submit" value="Search">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>

                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama
                                        costumer</th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Status</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Grand Total</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Metode bayar</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Wakru Order</th>
                                    <th class="text-secondary opacity-7 ps-2"> Action</th>
                                </tr>
                            </thead>
                            <?php $no = 1;
                            // var_dump($item);
                            foreach ($order['data'] as $ord) : ?>
                            <tbody>
                                <tr>
                                    <td>
                                        <div><?= $no++ ?></div>
                                    </td>
                                    <td>
                                        <h6 class="mb-0 text-sm"><?= $ord['nama_customer'] ?></h6>
                                    </td>
                                    <td class=" text-center text-sm">
                                        <h6 class=" <?= $ord['status_cucian'] ?> mb-0 text-sm">
                                            <?= $ord['status_cucian'] ?>
                                        </h6>
                                    </td>
                                    <td>
                                        <h6 class="mb-0 text-sm"><?= $ord['total_harga'] ?></h6>
                                    </td>
                                    <td>
                                        <h6 class="mb-0 text-sm"><?= $ord['metode_pembayaran'] ?></h6>
                                    </td>
                                    <td>
                                        <h6 class="mb-0 text-sm"><?= $ord['waktu_cucian_masuk'] ?></h6>
                                    </td>
                                    <td>
                                        <a href="<?= base_url('riwayat/invoice/' . $ord['id_cucian']) ?>"><span>Lihat
                                                invoice...</span></a>
                                    </td>
                                    <td> <a href="<?= base_url('riwayat/delete/' . $ord['id_cucian']) ?>"
                                            class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a></td>
                                </tr>
                            </tbody>
                            <?php endforeach ?>
                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-right">
                                <?php echo $pagination; ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
