<div class="col-lg-6 col-7">
    <?php echo $this->session->flashdata('message'); ?>
</div>
<form action="<?= base_url("profile/edit"); ?>" method="POST" enctype="multipart/form-data">
    <div class="container-fluid py-4">
        <div class="row container">
            <div class="col-md-8">
                <div class="card">
                    <?php
                    foreach ($data as $ro) : ?>
                        <div class="card-header pb-0">
                            <div class="d-flex align-items-center">
                                <p class="mb-0">Profile</p>
                                <button type="submit" class="btn btn-info btn-sm ms-auto" value="Simpan">Edit</button>

                            </div>
                        </div>
                        <div class="card-body">
                            <p class="text-uppercase text-sm">User Information</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Nama </label>
                                        <input type="hidden" name="id_admin" value="<?= $ro['id_admin'] ?>">
                                        <input class="form-control" type="text" value="<?= $ro['nama_admin'] ?>" name="nama_admin">

                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">No Telp</label>
                                        <input class="form-control" type="text" value="" name="">
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Email</label>
                                        <input class="form-control" type="email" value="<?= $ro['email'] ?>" name="email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Password</label>
                                        <input class="form-control" type="password" value="<?= $ro['password'] ?>" name="password">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</form>
