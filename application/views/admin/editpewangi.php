<?php foreach ($pewangi['data'] as $pw) : ?>
<form action="<?= base_url("pewangi/update"); ?>" method="POST" enctype="multipart/form-data">
    <div class="container-fluid py-2" style="display: flex; justify-content:center; width:50%;">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header pb-0">
                        <div class="d-flex align-items-center">
                            <p class="mb-0">Edit pewangi</p>
                            <a href="<?php echo base_url('pewangi/index') ?>" class="btn btn-info btn-sm ms-auto"><i
                                    class="fa fa-sign-out" aria-hidden="true"></i> Close</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <hr class="horizontal dark">
                        <p class="text-uppercase text-sm">Halaman Edit</p>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Nama pewangi</label>
                                    <input type="hidden" name="id_pewangi" value="<?= $pw['id_pewangi'] ?>">
                                    <input class="form-control" type="text" name="nama_pewangi"
                                        value="<?= $pw['nama_pewangi'] ?>">
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-success btn-sm" value="Simpan"><i class="fa fa-save"></i>
                            Simpan</button>
                        <button type="reset" class="btn btn-danger btn-sm"><i class="fa fa-save"></i>Reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php endforeach ?>