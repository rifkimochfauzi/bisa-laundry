<button class="btn btn-light text-capitalize border-0" data-mdb-ripple-color="dark" onclick="window.print()"><i
        class="fas fa-print text-info"></i>Print</button>
<style>
/* CSS untuk mencetak hanya div dengan class "print-content" */
@media print {
    body * {
        visibility: hidden;
        header: hidden;
    }

    .print-content,
    .print-content * {
        visibility: visible;
        size: A4;
        margin: 0;
        left: 200px
    }



}
</style>
<?php foreach ($cucian['data'] as $cc) : ?>

<div class="card" style="max-width: 650px; position:relative;left:50px;">
    <div class="print-content">
        <div class="card-body">
            <div class="container mb-5 mt-3">
                <div class="col-xl-9">
                    <p style="color: #7e8d9f;font-size: 20px;">Invoice >> <strong>ID:
                            <?= $cc['id_cucian'] ?></strong>
                    </p>
                </div>
                <hr>
            </div>
            <div class="container">
                <div class="col-md-12">
                    <div class="text-center">
                        <i class="fab fa- fa-4x ms-0" style="color:#5d9fc5 ;"></i>
                        <p class="pt-0" style="color:#5d9fc5 ;">BISA.LAUNDRY</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-8">
                        <label class="text-muted"> Jl. Sholeh Iskandar No. 22,Kedung Jaya, Kec. Tanah Sereal Kota
                            Bogor, Jawa Barat</label>
                        <label class="text-muted"><i class="fas fa-phone"></i> 123-456-789</label><br><br>
                        <label class="text-muted ms-3"><span class="text-black me-4">Total
                                :</span><?= $cc['total_harga'] ?></label><br>
                        <label class="text-muted ms-3 "><span class="text-black me-4">pembayaran
                                :</span><?= $cc['metode_pembayaran'] ?></label>
                    </div>
                    <div class="col-xl-4">
                        <p class="text-muted">Invoice</p>
                        <ul class="list-unstyled">
                            <label class="text-muted"><i class="fas fa-circle" style="color:#84B0CA ;"></i> <span
                                    class="fw-bold">Nama : </span><?= $cc['nama_customer'] ?><br><span
                                    class="fw-bold">No hp
                                    : </span><?= $cc['no_hp'] ?></label>
                            <label class="text-muted"><i class="fas fa-circle" style="color:#84B0CA ;"></i> <span
                                    class="fw-bold">Cucian Masuk: </span><?= $cc['waktu_cucian_masuk'] ?></label>
                            <label class="text-muted"><i class="fas fa-circle" style="color:#84B0CA ;"></i> <span
                                    class="me-1 fw-bold">Estimasi
                                    Selesai:<?= $cc['waktu_cucian_selesai'] ?></span></label>
                        </ul>
                    </div>
                </div>
                <div class="row my-2 mx-1 justify-content-center">
                    <table class="table table-striped table-borderless">
                        <thead style="background-color:#84B0CA ;" class="text-white">
                            <tr>
                                <th scope="col">Layanan Laundry</th>
                                <th scope="col">Berat (kg/pcs)</th>
                                <!-- <th scope="col">jenis pewangi</th> -->
                                <th scope="col">Total harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= $cc['nama_layanan'] ?></td>
                                <td><?= $cc['jumlah_layanan'] ?></td>
                                <!-- <th><?= $cc['id_pewangi'] ?></th> -->
                                <td><?= $cc['total_harga'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-xl-8">
                        <label>KETENTUAN :
                        </label>
                        <label>
                            1. Bisa Laundry tidak bertanggung jawab atas pakaian yang luntur yang tidak
                            diinformasikan
                            kepada kami.
                            2. Komplain pakaian kami layani 1x24 jam sejak pakaian diambil.
                        </label>
                        <label> 3. Pengambilan laundry wajib menggunakan nota asli.</label>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xl-10">
                        <p>Thank you for ordering and trusting us in choosing our services
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endforeach ?>