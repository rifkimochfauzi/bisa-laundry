<table id="datatable">
    <thead>
        <tr>
            <th>Column 1</th>
            <th>Column 2</th>
            <!-- Add more columns as needed -->
        </tr>
    </thead>
    <tbody></tbody>
</table>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#datatable').DataTable({
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url('progress1/get_api_data'); ?>",
            "type": "POST",
            "data": function(d) {
                d.start = d.start || 0;
                d.length = d.length || 10;
                // Add any additional parameters needed by the API
            }
        },
        "columns": [{
                "data": "column1"
            },
            {
                "data": "column2"
            },
            // Add more columns as needed
        ]
    });
});
</script>