<div class="col-lg-6 col-7">
    <?php echo $this->session->flashdata('message'); ?>
</div>
<div class="container"><br />
    <div class="row">
        <div class="col-lg-6 col-7">
            <h6>Pilih Satu Layanan</h6>
        </div>
        <div class=" col-lg-6 col-5 my-auto text-end">
            <form method="post" action="<?php echo base_url('pembayaran/search_data'); ?>">

                <select name="keyword" id="bhs">
                    <option value="satuan/pcs">satuan/pcs</option>
                    <option value="Kg">Kg</option>
                </select>
                <!-- <input type="text" name="keyword" placeholder="Type here..."> -->
                <input type="submit" value="filter">
            </form>
        </div>
        <div class=" col-lg-6 col-5 my-auto text-end">
            <form method="post" action="<?php echo base_url('pembayaran/search_data'); ?>">
                <input type="text" name="keyword" placeholder="Type here...">
                <input type="submit" value="Search">
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!-- <div class="card mb-4"> -->
            <div class="row">
                <?php foreach ($item['data'] as $itm) : ?>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <br>
                        <div class="card">
                            <div class="caption ">
                                <h6 style="text-align: center;"><?= $itm['nama_item'] ?>
                                    (<?= $itm['tipe_item'] ?>)</h6>
                                <div class="col-md-12">
                                    <h6 style="text-align: center;">harga : <?= $itm['harga'] ?></h6>
                                </div>
                                <h6 class="text-xs text-secondary mb-0">masukan berat cucian </h6>
                                <div class="col-md-12">
                                    <input type="number" name="quantity" id="<?= $itm['id_item'] ?>"
                                        class="quantity form-control">
                                </div>
                                <br>
                                <button class="add_cart btn btn-success btn-block"
                                    data-productid="<?= $itm['id_item'] ?>" data-productname="<?= $itm['nama_item'] ?>"
                                    data-productsatuan="<?= $itm['tipe_item'] ?>"
                                    data-productprice="<?= $itm['harga'] ?>">Add To Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-right">
                    <?php echo $pagination; ?>
                </ul>
            </nav>
            </div>
            <!-- </div> -->
        </div>
        <div class="col-md-4">
            <h4>pembayaran</h4>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Items</th>
                        <th>Price</th>
                        <th>Berat(kg/pcs)</th>
                        <th>Total</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody id="detail_cart">
                </tbody>
            </table>

        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/jquery-3.2.1.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js' ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.add_cart').click(function() {
        var id_item = $(this).data("productid");
        var nama_item = $(this).data("productname");
        var harga = $(this).data("productprice");
        var satuan = $(this).data("productsatuan");
        var quantity = $('#' + id_item).val();
        $.ajax({
            url: "<?php echo site_url('pembayaran/add_to_cart'); ?>",
            method: "POST",
            data: {
                id_item: id_item,
                nama_item: nama_item,
                harga: harga,
                satuan: satuan,
                quantity: quantity
            },
            success: function(data) {
                $('#detail_cart').html(data);
            }
        });
    });


    $('#detail_cart').load("<?php echo site_url('pembayaran/load_cart'); ?>");


    $(document).on('click', '.romove_cart', function() {
        var row_id = $(this).attr("id");
        $.ajax({
            url: "<?php echo site_url('pembayaran/delete_cart'); ?>",
            method: "POST",
            data: {
                row_id: row_id
            },
            success: function(data) {
                $('#detail_cart').html(data);
            }
        });
    });
    $(document).on('click', '.chekout_cart', function() {
        var data_layanan = $(this).attr("data-layanan");
        var data_qty = $(this).attr("data-qty");
        var data_uang = $(this).attr("data-uang");
        var data_satuan = $(this).attr("data-satuan");
        window.open("<?php echo base_url(); ?>riwayat/detailtrans?layanan=" + data_layanan +
            "&qty=" + data_qty + "&total=" + data_uang + "&satuan=" + data_satuan)
        // $.ajax({
        //     url: "<?php echo site_url('pembayaran/chekout_cart'); ?>",
        //     method: "POST",
        //     data: {
        //         data_kirim: data_uang,
        //         data_kirim1: data_layanan,
        //         data_kirim2: data_uang,
        //     },
        //     success: function(data) {
        //         // console.log()
        //         // $('.hasil_chekout_cart').html('hasil checkout : ' + data);
        //         window.open("<?php echo base_url(); ?>riwayat/detailtrans?total="+data)
        //     }
        // });
    });
});
</script>
