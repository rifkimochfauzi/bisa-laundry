<div class="card text-center">
    <div class="card-body">
        <h5 class="card-title">Lihat Laporan pembayaran dan keuangan</h5>
        <?php echo validation_errors(); ?>
        <?php echo form_open('pemasukan/generate'); ?>
        <p class="card-text">Masukan tanggal awal dan akhir laporan pembayaran yang ingin di lihat </p>
        <label for="start">Tanggal Awal:</label>
        <input type="date" id="start" name="start"><br><br>

        <label for="end">Tanggal Akhir:</label>
        <input type="date" id="end" name="end"><br><br>
        <input type="submit" value="Generate Laporan" class="btn btn-info btn-sm">
        </form>
    </div>

</div>