<?php
defined('BASEPATH') or exit('No direct script access allowed');

class riwayat extends CI_Controller

{
    function __construct()
    {
        parent::__construct();

        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        // if (!$this->session->access_token) {
        //     $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebihdahulu</div>');
        //     redirect('login');
        // }
    }
    public function index($page=1)
    {
         $curl = curl_init();
        $limit = 7;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?limit='.$limit.'&page='.$page.'&search=Dikembalikan',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $data['order'] = $response;

        // Pagination
        $total_rows = $response['offset']; // Ganti dengan jumlah total baris dari API Anda
        $config['base_url'] = base_url('riwayat/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $total_rows - 5;

         // Customize anchor and active link
         $config['full_tag_open'] = '<ul class="pagination">';        
         $config['full_tag_close'] = '</ul>';        
         $config['first_link'] = 'First';        
         $config['last_link'] = 'Last';        
         $config['first_tag_open'] = '<li class="page-item page-blue"><span class="page-link">';        
         $config['first_tag_close'] = '</span></li>';        
         $config['prev_link'] = '&laquo';        
         $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['prev_tag_close'] = '</span></li>';        
         $config['next_link'] = '&raquo';        
         $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['next_tag_close'] = '</span></li>';        
         $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['last_tag_close'] = '</span></li>';        
         $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';        
         $config['cur_tag_close'] = '</a></li>';        
         $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['num_tag_close'] = '</span></li>';
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        // $data['order'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_order'), true);
        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        $this->load->view('admin/riwayat', $data);
        $this->load->view('templates1/footer');
    }
    public function detailtrans()
    {
        $data['pewangi'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_pewangi'), true);
        $this->load->view('templates1/header');
        // $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/detailtrans', $data);
        $this->load->view('templates1/footer');
    }
    public function detairiwayattrans()
    {

        $this->load->view('templates1/header');
        // $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/detairiwayattrans');
        $this->load->view('templates1/footer');
    }
    public function invoice($id_cucian)
    {
        $where = array('id_cucian' => $id_cucian);
        $data['cucian'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_invoice', $where), true);
        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/invoice',$data);
        $this->load->view('templates1/footer');
    }
    public function print()
    {
    }
    public function delete($id_cucian)
    {

        $data = array(
            'id_cucian' => $id_cucian,
            'is_delete' => '1',
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/update_order',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Authorization: JWT ' . $this->session->access_token,
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

  
        echo $response;
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($code == 200) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">berhasil menghapus riwayat pesanan<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>');
            redirect('riwayat');
        }
        // Jika gagal 
        else {
            echo 'eror';
            // $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email Sudah terdaftar, Silahkan registrasi ulang, Pastikan data terisi dengan lengkap </div>');
            // redirect('login/regist');
            // lakukan sesuatu setelah gagal login
        }
    }
    public function search_data($page = 1)
    {
    $keyword = $this->input->post('keyword');
         $curl = curl_init();
        $limit = 7;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?search=' . urlencode($keyword).'&limit='.$limit.'&page='.$page,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $data['order'] = $response;

        // Pagination
        $total_rows = $response['offset']; // Ganti dengan jumlah total baris dari API Anda
        $config['base_url'] = base_url('riwayat/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $total_rows - 5;

         // Customize anchor and active link
         $config['full_tag_open'] = '<ul class="pagination">';        
         $config['full_tag_close'] = '</ul>';        
         $config['first_link'] = 'First';        
         $config['last_link'] = 'Last';        
         $config['first_tag_open'] = '<li class="page-item page-blue"><span class="page-link">';        
         $config['first_tag_close'] = '</span></li>';        
         $config['prev_link'] = '&laquo';        
         $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['prev_tag_close'] = '</span></li>';        
         $config['next_link'] = '&raquo';        
         $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['next_tag_close'] = '</span></li>';        
         $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['last_tag_close'] = '</span></li>';        
         $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';        
         $config['cur_tag_close'] = '</a></li>';        
         $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['num_tag_close'] = '</span></li>';
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
      


        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/riwayat', $data);
        $this->load->view('templates1/footer');
    }
}