<?php
defined('BASEPATH') or exit('No direct script access allowed');

class home extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->library('pagination');
        $this->load->helper('url');
        if (!$this->session->access_token) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebihdahulu</div>');
            redirect('login');
        }
    }

    public function index($page = 1)
    {

         $curl = curl_init();
        $limit = 4;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?limit='.$limit.'&page='.$page,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $data['order'] = $response;

        // Pagination
        $total_rows = $response['offset']; // Ganti dengan jumlah total baris dari API Anda
        $config['base_url'] = base_url('home/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $total_rows - 3;

         // Customize anchor and active link
         $config['full_tag_open'] = '<ul class="pagination">';        
    $config['full_tag_close'] = '</ul>';        
    $config['first_link'] = 'First';        
    $config['last_link'] = 'Last';        
    $config['first_tag_open'] = '<li class="page-item page-blue"><span class="page-link">';        
    $config['first_tag_close'] = '</span></li>';        
    $config['prev_link'] = '&laquo';        
    $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';        
    $config['prev_tag_close'] = '</span></li>';        
    $config['next_link'] = '&raquo';        
    $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';        
    $config['next_tag_close'] = '</span></li>';        
    $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';        
    $config['last_tag_close'] = '</span></li>';        
    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';        
    $config['cur_tag_close'] = '</a></li>';        
    $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';        
    $config['num_tag_close'] = '</span></li>';
        //  $config['num_links'] = 3;
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        // $data['order'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_order'), true);
        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/Dash', $data);
        $this->load->view('templates1/footer');
    }
    // public function tambah()
    // {
    //     $this->load->view('templates1/header');
    //     $this->load->view('admin/tambahitem');
    //     $this->load->view('templates1/footer');
    // }
    // public function edit()
    // {
    //     $this->load->view('templates1/header');
    //     $this->load->view('admin/edititem');
    //     $this->load->view('templates1/footer');
    // }
    //aksi proses update data 
    public function edit($id_cucian)
    {
        $where = array('id_cucian' => $id_cucian);
        $data['order'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_order', $where), true);
        $this->load->view('templates1/header');
        $this->load->view('admin/editstatus', $data);
        $this->load->view('templates1/footer');
    }
    public function update()
    {
  
        $data = array(
            'alamat_customer' => $this->input->post('alamat_customer'),
            'status_cucian' => $this->input->post('status_cucian'),
            'id_cucian' => $this->input->post('id_cucian'),
            // 'is_delete' => $this->input->post('status_cucian'),
            'metode_pembayaran' => $this->input->post('metode_pembayaran'),
            'nama_customer' => $this->input->post('nama_customer'),
            // 'total_harga' => $this->input->post('total_harga'),
            'no_hp' => $this->input->post('no_hp'),
            // 'waktu_cucian_selesai' => $this->input->post('waktu_cucian_selesai'),
            // 'waktu_cucian_masuk' => $this->input->post('waktu_cucian_masuk'),
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/update_order',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY4NTYzNTY3NCwianRpIjoiYzcyZGNiNGItNTJjNC00MDEyLWFjNTQtMDYwZDc1YTA0ZmU4IiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluMUBnbWFpbC5jb20iLCJuYmYiOjE2ODU2MzU2NzQsImV4cCI6MTY4ODIyNzY3NCwiaWRfYWRtaW4iOjEsInJvbGUiOjExLCJyb2xlX2Rlc2MiOiJBRE1JTiIsImVtYWlsIjoiYWRtaW4xQGdtYWlsLmNvbSJ9.IZj688L4b7wUevvgsmkx_UItQu_3ZY-W4Z2k7KMkiUI',
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($httpCode == 200) {
            redirect('home');
        } else {
            echo 'Gagal mengupdate data ke REST API. Error: ' . curl_error($curl);
        }
        curl_close($curl);
        // curl_close($curl);
        echo $response;
    }
    public function tambah_aksi()
    {

        // $nama_foto = $_FILES['nama_foto'];
        // if ($nama_foto = '') {
        // } else {
        //     $config['upload_path']      = './uploads/operator';
        //     $config['allowed_types']    = 'pdf|doc|docx|gif|jpg|png|jpeg';

        //     $this->load->library('upload', $config);
        //     if (!$this->upload->do_upload('nama_foto')) {
        //         echo "upload file Gagal";
        //         die();
        //     } else {
        //         $nama_foto = $this->upload->data('file_name');
        //     }
        // }
        // Membuat array data
        $data = array(
            'nama_pewangi' => $this->input->post('nama_pewangi'),

        );

        $cookiestore = tempnam(sys_get_temp_dir(), '_cookiejar_');
        $url = $this->API . '/laundry/insert_pewangi';
        $headers = array(
            'Authorization: JWT ' . $this->session->access_token,
            'Content-Type: application/json'
        );
        $options = array(
            CURLOPT_COOKIESESSION   =>  true,
            CURLOPT_COOKIEFILE      =>  $cookiestore,
            CURLOPT_COOKIEJAR       =>  $cookiestore,
            CURLOPT_POST            =>  true,
            CURLOPT_POSTFIELDS      =>  json_encode($data)
        );
        $res = $this->curl($url, $options, $headers);
        // echo json_encode($res);
        if ($res->status == 200) {
            printf('<pre>%s</pre>', print_r($res->response, true));
        }
    }
    function curl($url = NULL, $options = NULL, $headers = false)
    {
        $vbh = fopen('php://temp', 'w+');
        session_write_close();

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, trim($url));
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLINFO_HEADER_OUT, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0');
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_ENCODING, '');

        /* enhanced debug */
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_NOPROGRESS, true);
        curl_setopt($curl, CURLOPT_STDERR, $vbh);

        /* Assign runtime parameters as options to override defaults if needed. */
        if (isset($options) && is_array($options)) {
            foreach ($options as $param => $value) curl_setopt($curl, $param, $value);
        }

        /* send any headers with the request that are needed */
        if (isset($headers) && is_array($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        /* Execute the request and store responses */
        $res = (object)array(
            'response'  =>  curl_exec($curl),
            'status'    =>  curl_getinfo($curl, CURLINFO_RESPONSE_CODE),
            'info'      =>  (object)curl_getinfo($curl),
            'errors'    =>  curl_error($curl)
        );

        rewind($vbh);
        $res->verbose = stream_get_contents($vbh);
        fclose($vbh);
        curl_close($curl);
        redirect('home');
        return $res;
    }
    public function search_data($page = 1)
    {
        $keyword = $this->input->post('keyword');
        $curl = curl_init();
        $limit = 4;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?search=' . urlencode($keyword).'&limit='.$limit.'&page='.$page,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $data['order'] = $response;
       

        // $url = 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?search=' . urlencode($keyword);

        // $curl = curl_init($url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $response = curl_exec($curl);
        // curl_close($curl);

        // $data['order'] = json_decode($response, true);
        $total_rows = $response['offset']; // Ganti dengan jumlah total baris dari API Anda
        $config['base_url'] = base_url('home/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $total_rows - 3;

         // Customize anchor and active link
         $config['full_tag_open'] = '<ul class="pagination">';        
    $config['full_tag_close'] = '</ul>';        
    $config['first_link'] = 'First';        
    $config['last_link'] = 'Last';        
    $config['first_tag_open'] = '<li class="page-item page-blue"><span class="page-link">';        
    $config['first_tag_close'] = '</span></li>';        
    $config['prev_link'] = '&laquo';        
    $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';        
    $config['prev_tag_close'] = '</span></li>';        
    $config['next_link'] = '&raquo';        
    $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';        
    $config['next_tag_close'] = '</span></li>';        
    $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';        
    $config['last_tag_close'] = '</span></li>';        
    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';        
    $config['cur_tag_close'] = '</a></li>';        
    $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';        
    $config['num_tag_close'] = '</span></li>';
        //  $config['num_links'] = 3;
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();


        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/Dash', $data);
        $this->load->view('templates1/footer');
    }
}