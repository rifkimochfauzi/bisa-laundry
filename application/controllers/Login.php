<?php
defined('BASEPATH') or exit('No direct script access allowed');

class login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header');
            $this->load->view('templates/topbar');
            $this->load->view('login');
        } else {
            //validasi success,method privat
            $this->_login();
        }
        // $this->load->view('templates/footer');
    }

    private function _login()
    {

        $data = array(
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),

        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/login_admin',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
    
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // Jika berhasil login

        if ($code == 200 && $response !== false) {
            $response_data = json_decode($response, true);
            $access_token = $response_data['access_token'];
            $access_token = $this->session->set_userdata('access_token', $access_token);
            redirect('home');
            // lakukan sesuatu setelah berhasil login
        }
        // Jika gagal login
        else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email belum terdaftar,atau password salah Silahkan registrasi atau cek kembali password anda!</div>');
            redirect('login');
            // lakukan sesuatu setelah gagal login
        }
    }

    public function regist()
    {
        $this->form_validation->set_rules('nama_admin', 'Nama_admin', 'required|trim');
        // $this->form_validation->set_rules('alamat_customer', 'Alamat_customer', 'required|trim');
        // $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
        //     'is_unique' => 'This email has already registered!'
       

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header');
            $this->load->view('templates/topbar');
            $this->load->view('regist');
        } else {

            $data = [
                'nama_admin' => htmlspecialchars($this->input->post('nama_admin', true)),
                // 'alamat_customer' => $this->input->post('alamat_customer'),
                'email' => $this->input->post('email', true),
                'foto_user' => 'default.jpg',
                'password' => $this->input->post('password', true),
                'status_user' => 1,
                'waktu_daftar' => time()
            ];
            $this->session->set_userdata($data);
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/user/insert_admin',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);

			echo $response;

            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            // Jika berhasil login
            if ($code == 200) {
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Congratulation! your account has been created.</div>');
                redirect('login');
            }
            // Jika gagal login
            else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email Sudah terdaftar, Silahkan registrasi ulang, Pastikan data terisi dengan lengkap </div>');
                redirect('login/regist');
                // lakukan sesuatu setelah gagal login
            }
        }
    }

    public function logout()
    {
        // var_dump('keluar');

        $this->session->sess_destroy();

        redirect('login');
    }
    public function reset()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/topbar');
        $this->load->view('reset');
    }
}