<?php
defined('BASEPATH') or exit('No direct script access allowed');

class pembayaran extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        if (!$this->session->access_token) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebihdahulu</div>');
            redirect('login');
        }
    }

    function index($page=1)
    {
        $curl = curl_init();
        $limit = 9;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_item?limit='.$limit.'&page='.$page,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $data['item'] = $response;

        $total_rows = $response['offset']; // Ganti dengan jumlah total baris dari API Anda
        $config['base_url'] = base_url('pembayaran/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $total_rows - 7;

        // Customize anchor and active link
        $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '  <li class="page-item disabled">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<li class="page-item">Previous</li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<li class="page-item">Next</li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['num_links'] = 3;
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        // <!-- $data['item'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_item'), true); -->
        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/pembayaran', $data);
        $this->load->view('templates1/footer');
    }

    function add_to_cart()
    {
        $data = array(
            'id' => $this->input->post('id_item'),
            'name' => $this->input->post('nama_item'),
            'satuan' => $this->input->post('satuan'),
            'price' => $this->input->post('harga'),
            'qty' => $this->input->post('quantity'),
        );
        $this->cart->insert($data);
        echo $this->show_cart();
    }

    function show_cart()
    {
        $output = '';
        $no = 0;
        foreach ($this->cart->contents() as $items) {
            $no++;
            $output .= '
				<tr>
					<td>' . $items['name'] .'  '.$items['satuan'] .'</td>
					<td>' . number_format($items['price']) . '</td>
					<td>' . $items['qty'] . '</td>
					<td>' . number_format($items['subtotal']) . '</td>
					<td><button type="button" id="' . $items['rowid'] . '" class="romove_cart btn btn-danger btn-sm">Cancel</button></td>
				</tr>
			';
        }
        $output .= '
			<tr>
            <th colspan="">' . 'Layanan: '  . $items['name'] .  '</th>
				<th colspan="2">' . 'kg/pcs:' . $items['qty'] . '</th>
                <th colspan="">Total :</th>
                <th colspan="">' . 'Rp ' . number_format($this->cart->total()) . '</th>
		';
        $output .= '
        <tr>
         
        <th>
        <button type="button" 
                data-layanan="' . $items['name'] . '" 
                data-qty="' . $items['qty'] . '" 
                data-satuan="' . $items['satuan'] . '" 
                data-uang="' . $this->cart->total() . '" 
                class=" chekout_cart btn btn-info btn-center " 
                data-bs-toggle="modal" data-bs-target="#basicModal">
            Bayar
        </button>
        </th>
        </tr>
    ';

        return $output;
    }

    function load_cart()
    {
        echo $this->show_cart();
    }

    function delete_cart()
    {
        $data = array(
            'rowid' => $this->input->post('row_id'),
            'qty' => 0,
        );
        $this->cart->update($data);
        echo $this->show_cart();
    }

    function chekout_cart()
    {
        $data = $this->input->post('data_kirim');
        // $data1 = $this->input->post('data_kirim1');
        // $data2 = $this->input->post('data_kirim2');
        echo 'Rp. ' . $data;
    }
    public function tambah_aksi()
    {

        // $nama_foto = $_FILES['nama_foto'];
        // if ($nama_foto = '') {
        // } else {
        //     $config['upload_path']      = './uploads/operator';
        //     $config['allowed_types']    = 'pdf|doc|docx|gif|jpg|png|jpeg';

        //     $this->load->library('upload', $config);
        //     if (!$this->upload->do_upload('nama_foto')) {
        //         echo "upload file Gagal";
        //         die();
        //     } else {
        //         $nama_foto = $this->upload->data('file_name');
        //     }
        // }
        // Membuat array data
        $data = array(
            // 
            'nama_customer' => $this->input->post('nama_customer'),
            'nama_layanan' => $this->input->post('nama_layanan'),
            'jumlah_layanan' => $this->input->post('jumlah_layanan'),
            'alamat_customer' => $this->input->post('alamat_customer'),
            'total_harga' => $this->input->post('total_harga'),
            'no_hp' => $this->input->post('no_hp'),
            'id_pewangi' => $this->input->post('id_pewangi'),
            'metode_pembayaran' =>  $this->input->post('metode_pembayaran'),
            'status_cucian' =>  $this->input->post('status_cucian'),
            'waktu_cucian_selesai' =>  $this->input->post('waktu_cucian_selesai'),
            'waktu_cucian_masuk' => time(),

        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/insert_order',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY4NTYzNTY3NCwianRpIjoiYzcyZGNiNGItNTJjNC00MDEyLWFjNTQtMDYwZDc1YTA0ZmU4IiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluMUBnbWFpbC5jb20iLCJuYmYiOjE2ODU2MzU2NzQsImV4cCI6MTY4ODIyNzY3NCwiaWRfYWRtaW4iOjEsInJvbGUiOjExLCJyb2xlX2Rlc2MiOiJBRE1JTiIsImVtYWlsIjoiYWRtaW4xQGdtYWlsLmNvbSJ9.IZj688L4b7wUevvgsmkx_UItQu_3ZY-W4Z2k7KMkiUI',
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        echo $response;
         $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // Jika berhasil login

        if ($code == 200 ) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"" role="alert " >
            Pembayaran Berhasil !
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>');
           
            redirect('progress');
            // lakukan sesuatu setelah berhasil login
        }
        // Jika gagal login
        else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">pembayaran gagal</div>');
            redirect('pembayaran');
            // lakukan sesuatu setelah gagal login
        }

        // $cookiestore = tempnam(sys_get_temp_dir(), '_cookiejar_');
        // $url = $this->API . '/laundry/order_laundry';
        // $headers = array(
        //     'Authorization: JWT ' . $this->session->access_token,
        //     'Content-Type: application/json'
        // );
        // $options = array(
        //     CURLOPT_COOKIESESSION   =>  true,
        //     CURLOPT_COOKIEFILE      =>  $cookiestore,
        //     CURLOPT_COOKIEJAR       =>  $cookiestore,
        //     CURLOPT_POST            =>  true,
        //     CURLOPT_POSTFIELDS      =>  json_encode($data)
        // );
        // $res = $this->curl($url, $options, $headers);
        // // echo json_encode($res);
        // if ($res->status == 200) {
        //     printf('<pre>%s</pre>', print_r($res->response, true));
        // }
    }
    // function curl($url = NULL, $options = NULL, $headers = false)
    // {
    //     $vbh = fopen('php://temp', 'w+');
    //     session_write_close();

    //     $curl = curl_init();
    //     curl_setopt($curl, CURLOPT_URL, trim($url));
    //     curl_setopt($curl, CURLOPT_AUTOREFERER, true);
    //     curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    //     curl_setopt($curl, CURLOPT_FAILONERROR, true);
    //     curl_setopt($curl, CURLOPT_HEADER, false);
    //     curl_setopt($curl, CURLINFO_HEADER_OUT, false);
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
    //     curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
    //     curl_setopt($curl, CURLOPT_TIMEOUT, 60);
    //     curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0');
    //     curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
    //     curl_setopt($curl, CURLOPT_ENCODING, '');

    //     /* enhanced debug */
    //     curl_setopt($curl, CURLOPT_VERBOSE, true);
    //     curl_setopt($curl, CURLOPT_NOPROGRESS, true);
    //     curl_setopt($curl, CURLOPT_STDERR, $vbh);

    //     /* Assign runtime parameters as options to override defaults if needed. */
    //     if (isset($options) && is_array($options)) {
    //         foreach ($options as $param => $value) curl_setopt($curl, $param, $value);
    //     }

    //     /* send any headers with the request that are needed */
    //     if (isset($headers) && is_array($headers)) {
    //         curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    //     }

    //     /* Execute the request and store responses */
    //     $res = (object)array(
    //         'response'  =>  curl_exec($curl),
    //         'status'    =>  curl_getinfo($curl, CURLINFO_RESPONSE_CODE),
    //         'info'      =>  (object)curl_getinfo($curl),
    //         'errors'    =>  curl_error($curl)
    //     );

    //     rewind($vbh);
    //     $res->verbose = stream_get_contents($vbh);
    //     fclose($vbh);
    //     curl_close($curl);
    //     // redirect('progress');
    //     return $res;
    // }
    public function search_data()
    {
        $keyword = $this->input->post('keyword');

        $url = 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_item?search=' . urlencode($keyword);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);

        $data['item'] = json_decode($response, true);


        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/pembayaran', $data);
        $this->load->view('templates1/footer');
    }
}

ini_set('display_errors', 'off');
