<?php
defined('BASEPATH') or exit('No direct script access allowed');

class progress1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->library('pagination');
        $this->load->helper('url');
        if (!$this->session->access_token) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebihdahulu</div>');
            redirect('login');
        }
    }
    public function index()
    {
        $this->load->view('admin/progress1');
    }
    public function update()
    {
        $data = array(
            // 'alamat_customer' => $this->input->post('alamat_customer'),
            'status_cucian' => $this->input->post('status_cucian'),
            'id_cucian' => $this->input->post('id_cucian'),
            // 'is_delete' => $this->input->post('status_cucian'),
            // 'metode_pembayaran' => $this->input->post('metode_pembayaran'),
            // 'nama_customer' => $this->input->post('nama_customer'),
            // 'total_harga' => $this->input->post('total_harga'),
            // 'no_hp' => $this->input->post('no_hp'),
            // 'waktu_cucian_selesai' => $this->input->post('waktu_cucian_selesai'),
            // 'waktu_cucian_masuk' => $this->input->post('waktu_cucian_masuk'),
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/update_order',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY4NTYzNTY3NCwianRpIjoiYzcyZGNiNGItNTJjNC00MDEyLWFjNTQtMDYwZDc1YTA0ZmU4IiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluMUBnbWFpbC5jb20iLCJuYmYiOjE2ODU2MzU2NzQsImV4cCI6MTY4ODIyNzY3NCwiaWRfYWRtaW4iOjEsInJvbGUiOjExLCJyb2xlX2Rlc2MiOiJBRE1JTiIsImVtYWlsIjoiYWRtaW4xQGdtYWlsLmNvbSJ9.IZj688L4b7wUevvgsmkx_UItQu_3ZY-W4Z2k7KMkiUI',
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($httpCode == 200) {
            redirect('progress');
        } else {
            echo 'Gagal mengupdate data ke REST API. Error: ' . curl_error($curl);
        }
        curl_close($curl);
        // curl_close($curl);
        echo $response;
    }
    public function search_data()
    {
        $keyword = $this->input->post('keyword');

        $url = 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?search=' . urlencode($keyword);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);

        $data['order'] = json_decode($response, true);


        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/progress1', $data);
        $this->load->view('templates1/footer');
    }

    public function get_api_data() {
        $curl = curl_init();
        $limit = 5;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?limit='.$limit.'&page='.$page,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
    
        $requestData = $this->input->post();
    
        $requestData['start'] = $requestData['start'] ?? 0;
        $requestData['length'] = $requestData['length'] ?? 10;
        // Add any additional parameters needed by the API
    
        // Make the API request with the modified pagination parameters
        // $response = // Send the API request and retrieve the response
    
        echo json_encode($response);
    }
    
    
}