<?php
defined('BASEPATH') or exit('No direct script access allowed');

class progress extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->library('pagination');
        $this->load->helper('url');
        if (!$this->session->access_token) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebihdahulu</div>');
            redirect('login');
        }
    }
    public function index($page = 1)
    {
        $curl = curl_init();
        $limit = 5;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?limit='.$limit.'&page='.$page,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $data['order'] = $response;

        // Pagination
        $total_rows = $response['offset']; // Ganti dengan jumlah total baris dari API Anda
        $config['base_url'] = base_url('progress/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $total_rows - 3;

         // Customize anchor and active link
         $config['full_tag_open'] = '<ul class="pagination">';        
         $config['full_tag_close'] = '</ul>';        
         $config['first_link'] = 'First';        
         $config['last_link'] = 'Last';        
         $config['first_tag_open'] = '<li class="page-item page-blue"><span class="page-link">';        
         $config['first_tag_close'] = '</span></li>';        
         $config['prev_link'] = '&laquo';        
         $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['prev_tag_close'] = '</span></li>';        
         $config['next_link'] = '&raquo';        
         $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['next_tag_close'] = '</span></li>';        
         $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['last_tag_close'] = '</span></li>';        
         $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';        
         $config['cur_tag_close'] = '</a></li>';        
         $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['num_tag_close'] = '</span></li>';
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        
        // $data['order'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_order'), true);
        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/progress', $data);
        $this->load->view('templates1/footer');
    }
    public function edit($id_cucian)
    {
        $where = array('id_cucian' => $id_cucian);
        $data['order'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_order', $where), true);
        $this->load->view('templates1/header');
        $this->load->view('admin/editstatus1', $data);
        $this->load->view('templates1/footer');
    }
    public function update()
    {
  
        $data = array(
            'alamat_customer' => $this->input->post('alamat_customer'),
            'status_cucian' => $this->input->post('status_cucian'),
            'id_cucian' => $this->input->post('id_cucian'),
            // 'is_delete' => $this->input->post('status_cucian'),
            'metode_pembayaran' => $this->input->post('metode_pembayaran'),
            'nama_customer' => $this->input->post('nama_customer'),
            // 'total_harga' => $this->input->post('total_harga'),
            'no_hp' => $this->input->post('no_hp'),
            // 'waktu_cucian_selesai' => $this->input->post('waktu_cucian_selesai'),
            // 'waktu_cucian_masuk' => $this->input->post('waktu_cucian_masuk'),
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/update_order',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY4NTYzNTY3NCwianRpIjoiYzcyZGNiNGItNTJjNC00MDEyLWFjNTQtMDYwZDc1YTA0ZmU4IiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluMUBnbWFpbC5jb20iLCJuYmYiOjE2ODU2MzU2NzQsImV4cCI6MTY4ODIyNzY3NCwiaWRfYWRtaW4iOjEsInJvbGUiOjExLCJyb2xlX2Rlc2MiOiJBRE1JTiIsImVtYWlsIjoiYWRtaW4xQGdtYWlsLmNvbSJ9.IZj688L4b7wUevvgsmkx_UItQu_3ZY-W4Z2k7KMkiUI',
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($httpCode == 200) {
            redirect('progress');
        } else {
            echo 'Gagal mengupdate data ke REST API. Error: ' . curl_error($curl);
        }
        curl_close($curl);
        // curl_close($curl);
        echo $response;
    }
    public function search_data($page = 1)
    {
        $keyword = $this->input->post('keyword');

        // $url = 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?search=' . urlencode($keyword);

        // $curl = curl_init($url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $response = curl_exec($curl);
        // curl_close($curl);

        // $data['order'] = json_decode($response, true);
        
        $curl = curl_init();
        $limit = 5;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_order?search=' . urlencode($keyword).'&limit='.$limit.'&page='.$page,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $data['order'] = $response;

        // Pagination
        $total_rows = $response['offset']; // Ganti dengan jumlah total baris dari API Anda
        $config['base_url'] = base_url('progress/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $total_rows - 3;

         // Customize anchor and active link
         $config['full_tag_open'] = '<ul class="pagination">';        
         $config['full_tag_close'] = '</ul>';        
         $config['first_link'] = 'First';        
         $config['last_link'] = 'Last';        
         $config['first_tag_open'] = '<li class="page-item page-blue"><span class="page-link">';        
         $config['first_tag_close'] = '</span></li>';        
         $config['prev_link'] = '&laquo';        
         $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['prev_tag_close'] = '</span></li>';        
         $config['next_link'] = '&raquo';        
         $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['next_tag_close'] = '</span></li>';        
         $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['last_tag_close'] = '</span></li>';        
         $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';        
         $config['cur_tag_close'] = '</a></li>';        
         $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';        
         $config['num_tag_close'] = '</span></li>';
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/progress', $data);
        $this->load->view('templates1/footer');
    }
}