<?php
defined('BASEPATH') or exit('No direct script access allowed');

class riwayatcs extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        if (!$this->session->access_token) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebihdahulu</div>');
            redirect('logincs');
        }
    }
    public function index()
    {

        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar2');
        $this->load->view('costumer/riwayat');
        $this->load->view('templates1/footer');
    }
    // public function detailtrans()
    // {

    //     $this->load->view('templates1/header');
    //     // $this->load->view('templates1/sidebar');
    //     // $this->load->view('templates/topbar');
    //     $this->load->view('admin/detailtrans');
    //     $this->load->view('templates1/footer');
    // }
}
