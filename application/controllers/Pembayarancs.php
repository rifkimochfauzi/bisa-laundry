<?php
defined('BASEPATH') or exit('No direct script access allowed');

class pembayarancs extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        if (!$this->session->access_token) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebihdahulu</div>');
            redirect('logincs');
        }
    }

    function index()
    {
        $data1['data1'] = $this->product_modelcs->get_all_product();
        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar2');
        // $this->load->view('templates/topbar');
        $this->load->view('costumer/pembayaran', $data1);
        $this->load->view('templates1/footer');
    }

    function add_to_()
    {
        $data1 = array(
            'id' => $this->input->post('id_item'),
            'name' => $this->input->post('nama_item'),
            'price' => $this->input->post('harga'),
            'qty' => $this->input->post('quantity'),
        );
        $this->cart->insert($data1);
        echo $this->show_();
    }

    function show_()
    {
        $output = '';
        $no = 0;
        foreach ($this->cart->contents() as $item) {
            $no++;
            $output .= '
				<tr>
					<td>' . $item['name'] . '</td>
					<td>' . number_format($item['price']) . '</td>
					<td>' . $item['qty'] . '</td>
					<td>' . number_format($item['subtotal']) . '</td>
					<td><button type="button" id="' . $item['rowid'] . '" class="romove_cart btn btn-danger btn-sm">Cancel</button></td>
				</tr>
			';
        }
        $output .= '
			<tr>
				<th colspan="3">Total</th>
				<th colspan="2">' . 'Rp ' . number_format($this->cart->total()) . '</th>
             
		';
        $output .= '
        <tr>
         
         <th>
            <button type="button" id="' . number_format($this->cart->total()) . '" class="chekout_cart btn btn-info btn-sm">chekout</button></th>
        </tr>
    ';

        return $output;
    }

    function load_()
    {
        echo $this->show_();
    }

    function delete_()
    {
        $data1 = array(
            'rowid1' => $this->input->post('row_id1'),
            'qty' => 0,
        );
        $this->cart->update($data1);
        echo $this->show_();
    }
}
