<?php
defined('BASEPATH') or exit('No direct script access allowed');

class pemasukan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
        if (!$this->session->access_token) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebih dahulu</div>');
            redirect('login');
        }
    }
    public function index()
    {
       // $data['pemasukan'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_laporan'), true);
        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        $this->load->view('templates/topbar');
        $this->load->view('admin/pemasukan'); // Load view dan kirimkan data ke view
        $this->load->view('templates1/footer');
    }
    public function generate()
    {
        $start = $this->input->post('start');
        $end = $this->input->post('end');

        // Validasi tanggal awal dan akhir
        $this->load->library('form_validation');
        $this->form_validation->set_rules('start', 'Tanggal Awal', 'required');
        $this->form_validation->set_rules('end', 'Tanggal Akhir', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            // Jika validasi gagal, tampilkan kembali form dengan pesan error
            $this->load->view('admin/pemasukan');
        }
        else
        {
            // Panggil API menggunakan CURL
            $url = 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_laporan?start=' . $start . '&end=' . $end;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

            // Tampilkan hasil response dari API dalam view
            $data['laporan'] = json_decode($response, true);
            $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        $this->load->view('templates/topbar');
            $this->load->view('admin/laporan_keuangan_result', $data);
                   $this->load->view('templates1/footer');
        }
    }
}