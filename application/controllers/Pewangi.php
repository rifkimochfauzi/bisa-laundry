<?php
defined('BASEPATH') or exit('No direct script access allowed');

class pewangi extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        if (!$this->session->access_token) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebih dahulu</div>');
            redirect('login');
        }
    }

    //tampil data 
    public function index($page = 1)
    {
		$curl = curl_init();
        // $currentPage = 
        $limit = 5;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_pewangi?limit='.$limit.'&page='.$page,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $data['pewangi'] = $response;

        // Pagination
        $total_rows = $response['offset']; // Ganti dengan jumlah total baris dari API Anda
        $config['base_url'] = base_url('pewangi/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $total_rows - 4;

        // Customize anchor and active link
        $config['full_tag_open'] = '<ul class="pagination">';        
        $config['full_tag_close'] = '</ul>';        
        $config['first_link'] = 'First';        
        $config['last_link'] = 'Last';        
        $config['first_tag_open'] = '<li class="page-item page-blue"><span class="page-link">';        
        $config['first_tag_close'] = '</span></li>';        
        $config['prev_link'] = '&laquo';        
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';        
        $config['prev_tag_close'] = '</span></li>';        
        $config['next_link'] = '&raquo';        
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';        
        $config['next_tag_close'] = '</span></li>';        
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';        
        $config['last_tag_close'] = '</span></li>';        
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';        
        $config['cur_tag_close'] = '</a></li>';        
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';        
        $config['num_tag_close'] = '</span></li>';
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        // $data['item'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_item'), true);
        // var_dump($data['item'])

        // $data['pewangi'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_pewangi'), true);
        // var_dump($data['item'])
        // die;

        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        $this->load->view('templates/topbar');
        $this->load->view('admin/pewangi', $data); // Load view dan kirimkan data ke view
        $this->load->view('templates1/footer');
    }

    //select get pewangi 
    public function getselect()
    {
        $url = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_pewangi"; // Ganti dengan URL REST API Anda
        $ch = curl_init();

        // Set URL target
        curl_setopt($ch, CURLOPT_URL, $url);

        // Set return transfer to true
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Execute the request
        $response = curl_exec($ch);

        // Close cURL resource
        curl_close($ch);

        // Handle the response
        $data = json_decode($response, true);
        $this->load->view('templates1/header');
        // $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/pembayar', array('data' => $data)); // Load view dan kirimkan data ke view
        $this->load->view('templates1/footer');
    }

    //redirect ke view tambah 
    public function tambah()
    {
        $this->load->view('templates1/header');
        $this->load->view('admin/tambahitem');
        $this->load->view('templates1/footer');
    }
    //set rules agar saat menginputkan tidak kosong 
    public function _rules()
    {
        $this->form_validation->set_rules('nama_item', 'Nama item', 'required', array(
            'required' => '%s Harus di isi'
        ));
        $this->form_validation->set_rules('harga', 'harga item', 'required', array(
            'required' => '%s Harus di isi'
        ));
    }
    //aksi tambah ke database 
    public function tambah_aksi()
    {

        // $nama_foto = $_FILES['nama_foto'];
        // if ($nama_foto = '') {
        // } else {
        //     $config['upload_path']      = './uploads/operator';
        //     $config['allowed_types']    = 'pdf|doc|docx|gif|jpg|png|jpeg';

        //     $this->load->library('upload', $config);
        //     if (!$this->upload->do_upload('nama_foto')) {
        //         echo "upload file Gagal";
        //         die();
        //     } else {
        //         $nama_foto = $this->upload->data('file_name');
        //     }
        // }
        // Membuat array data
        $data = array(
            'nama_pewangi' => $this->input->post('nama_pewangi'),
        );

        $cookiestore = tempnam(sys_get_temp_dir(), '_cookiejar_');
        $url = $this->API . '/laundry/insert_pewangi';
        $headers = array(
            'Authorization: JWT ' . $this->session->access_token,
            'Content-Type: application/json'
        );
        $options = array(
            CURLOPT_COOKIESESSION   =>  true,
            CURLOPT_COOKIEFILE      =>  $cookiestore,
            CURLOPT_COOKIEJAR       =>  $cookiestore,
            CURLOPT_POST            =>  true,
            CURLOPT_POSTFIELDS      =>  json_encode($data)
        );
        $res = $this->curl($url, $options, $headers);
        // echo json_encode($res);
        if ($res->status == 200) {
            printf('<pre>%s</pre>', print_r($res->response, true));
        }
    }
    function curl($url = NULL, $options = NULL, $headers = false)
    {
        $vbh = fopen('php://temp', 'w+');
        session_write_close();

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, trim($url));
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLINFO_HEADER_OUT, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0');
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_ENCODING, '');

        /* enhanced debug */
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_NOPROGRESS, true);
        curl_setopt($curl, CURLOPT_STDERR, $vbh);

        /* Assign runtime parameters as options to override defaults if needed. */
        if (isset($options) && is_array($options)) {
            foreach ($options as $param => $value) curl_setopt($curl, $param, $value);
        }

        /* send any headers with the request that are needed */
        if (isset($headers) && is_array($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        /* Execute the request and store responses */
        $res = (object)array(
            'response'  =>  curl_exec($curl),
            'status'    =>  curl_getinfo($curl, CURLINFO_RESPONSE_CODE),
            'info'      =>  (object)curl_getinfo($curl),
            'errors'    =>  curl_error($curl)
        );

        rewind($vbh);
        $res->verbose = stream_get_contents($vbh);
        fclose($vbh);
        curl_close($curl);
        redirect('pewangi');
        return $res;
    }



    //redirect ke view edit 
    public function edit($id_pewangi)
    {
        $where = array('id_pewangi' => $id_pewangi);
        $data['pewangi'] = json_decode($this->curl->simple_get($this->API . '/laundry/get_pewangi', $where), true);
        $this->load->view('templates1/header');
        $this->load->view('admin/editpewangi', $data);
        $this->load->view('templates1/footer');
    }

    //aksi proses update data 
    public function update()
    {

        $data = array(
            'id_pewangi' => $this->input->post('id_pewangi'),
            'nama_pewangi' => $this->input->post('nama_pewangi'),
            // "is_delete" => '0',
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/update_pewangi',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Authorization: JWT ' . $this->session->access_token,
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        redirect('pewangi');
    }
    //aksi delete
    public function delete($id_pewangi)
    {

        $data = array(
            'id_pewangi' => $id_pewangi,
            'is_delete' => '1',
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/update_pewangi',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Authorization: JWT ' . $this->session->access_token,
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        
        echo $response;
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($code == 200) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">berhasill Menghapus pewangi</div>');
            redirect('pewangi');
        }
        // Jika gagal 
        else {
            echo 'eror';
            // $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email Sudah terdaftar, Silahkan registrasi ulang, Pastikan data terisi dengan lengkap </div>');
            // redirect('login/regist');
            // lakukan sesuatu setelah gagal login
        }
    }

    //search
    public function search_data($page = 1)
    {
        $keyword = $this->input->post('keyword');
        $curl = curl_init();
        // $currentPage = 
        $limit = 5;
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_pewangi?search=' . urlencode($keyword).'&limit='.$limit.'&page='.$page,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $data['pewangi'] = $response;

        // Pagination
        $total_rows = $response['offset']; // Ganti dengan jumlah total baris dari API Anda
        $config['base_url'] = base_url('pewangi/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $total_rows - 4;

        // Customize anchor and active link
        $config['full_tag_open'] = '<ul class="pagination">';        
        $config['full_tag_close'] = '</ul>';        
        $config['first_link'] = 'First';        
        $config['last_link'] = 'Last';        
        $config['first_tag_open'] = '<li class="page-item page-blue"><span class="page-link">';        
        $config['first_tag_close'] = '</span></li>';        
        $config['prev_link'] = '&laquo';        
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';        
        $config['prev_tag_close'] = '</span></li>';        
        $config['next_link'] = '&raquo';        
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';        
        $config['next_tag_close'] = '</span></li>';        
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';        
        $config['last_tag_close'] = '</span></li>';        
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';        
        $config['cur_tag_close'] = '</a></li>';        
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';        
        $config['num_tag_close'] = '</span></li>';
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        // $url = 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/laundry/get_pewangi?search=' . urlencode($keyword);

        // $curl = curl_init($url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $response = curl_exec($curl);
        // curl_close($curl);

        // $data['pewangi'] = json_decode($response, true);


        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/pewangi', $data);
        $this->load->view('templates1/footer');
    }
}