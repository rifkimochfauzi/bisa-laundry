<?php
defined('BASEPATH') or exit('No direct script access allowed');

class profile extends CI_Controller
{


    function __construct()
    {
        parent::__construct();

        $this->API = "https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        if (!$this->session->access_token) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">login terlebihdahulu</div>');
            redirect('login');
        }
    }

    //tampil data 
    public function index()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/user/get_my_profile_admin',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: JWT ' . $this->session->access_token,
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response, true);
        $this->load->view('templates1/header');
        $this->load->view('templates1/sidebar');
        // $this->load->view('templates/topbar');
        $this->load->view('admin/profile', $data);
        $this->load->view('templates1/footer');
    }
    //redirect ke view edit 
    public function edit()
    {
        $data = array(
            'id_admin' => $this->input->post('id_admin'),
            'nama_admin' => $this->input->post('nama_admin'),
            'email' => $this->input->post('email'),
            // 'password' => $this->input->post('password'),
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://stag-msib-01.bisa.ai:8080/backend_bisa_laundry/user/update_my_profile_admin',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: JWT ' . $this->session->access_token,
            ),
        ));

        $response = curl_exec($curl);


        echo $response;
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($code == 200) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">berhasil mengedit profile<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>');
            redirect('profile');
        }
        // Jika gagal 
        else {
            echo 'eror';
            // $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email Sudah terdaftar, Silahkan registrasi ulang, Pastikan data terisi dengan lengkap </div>');
            // redirect('login/regist');
            // lakukan sesuatu setelah gagal login
        }
    }

    //aksi proses update data 
    public function update()
    {
    }
}
